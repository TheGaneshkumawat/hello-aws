FROM maven:3-jdk-8-alpine 

FROM openjdk:8-jre-alpine

COPY build/*.jar /app.jar

EXPOSE 80

ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
